
<!-- <h1 align="center">AutoScriptVPS<img src="https://img.shields.io/badge/Version-2.0-blue.svg"></h1>

<p align="center">AutoScriptVPS is made by _Dreyannz_ to minimize the time consumed and user involvement in setting up your VPS</p> -->
## Supported Linux Distribution
Debian 8 & 9

<!-- <h3 align="center">Services</h3>
<p align="center">
  <a><img src="https://img.shields.io/badge/Service-OpenSSH-green.svg"></a>
  <a><img src="https://img.shields.io/badge/Service-Dropbear-green.svg"></a>
  <a><img src="https://img.shields.io/badge/Service-Stunnel-green.svg"></a>
  <a><img src="https://img.shields.io/badge/Service-OpenVPN-green.svg"></a>
  <a><img src="https://img.shields.io/badge/Service-Squid3-green.svg"></a>
 </p>
<h3 align="center">Commands</h3>
<p align="center">
  <a><img src="https://img.shields.io/badge/Commands-menu-yellow.svg"></a>
  <a><img src="https://img.shields.io/badge/Commands-accounts-yellow.svg"></a>
  <a><img src="https://img.shields.io/badge/Commands-options-yellow.svg"></a>
  <a><img src="https://img.shields.io/badge/Commands-server-yellow.svg"></a>
 </p> -->

## Installation

```bash
sudo su
wget -N https://gitlab.com/hugocollier/vpsroot/raw/main/root.sh && bash root.sh
```

```bash
apt update && apt upgrade -y && update-grub && sleep 2 && reboot
```


```bash
wget -O AutoScriptVPS https://gitlab.com/hugocollier/autoscriptvps/-/raw/main/AutoScriptVPS
chmod +x AutoScriptVPS
./AutoScriptVPS
```

<h3 align="center">Screenshots</h3>
<p align="center">
<img src="https://gitlab.com/hugocollier/autoscriptvps/-/raw/main/files/Screenshots/1.JPG">
</p>
<p align="center">
<img src="https://gitlab.com/hugocollier/autoscriptvps/-/raw/main/files/Screenshots/2.JPG">
</p>
<p align="center">
<img src="https://gitlab.com/hugocollier/autoscriptvps/-/raw/main/files/Screenshots/3.JPG">
</p>
<p align="center">
<img src="https://gitlab.com/hugocollier/autoscriptvps/-/raw/main/files/Screenshots/4.JPG">
</p>
